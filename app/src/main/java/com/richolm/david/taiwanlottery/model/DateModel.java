package com.richolm.david.taiwanlottery.model;

/**
 * Created by Kupo on 2016/09/18.
 */
public class DateModel {
    private int year;
    private int month;

    public DateModel(int year, int month) {
        this.year = year;
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }
}
