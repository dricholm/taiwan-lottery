package com.richolm.david.taiwanlottery.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.richolm.david.taiwanlottery.R;
import com.richolm.david.taiwanlottery.activities.ScannerActivity;
import com.richolm.david.taiwanlottery.adapters.ReceiptAdapter;
import com.richolm.david.taiwanlottery.contracts.ReceiptListContract;
import com.richolm.david.taiwanlottery.database.DbProvider;
import com.richolm.david.taiwanlottery.decorations.DividerItemDecoration;
import com.richolm.david.taiwanlottery.dialogs.ReceiptDialog;
import com.richolm.david.taiwanlottery.listeners.MainActivityListener;
import com.richolm.david.taiwanlottery.listeners.ReceiptListener;
import com.richolm.david.taiwanlottery.model.Receipt;
import com.richolm.david.taiwanlottery.presenters.ReceiptListPresenter;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Kupo on 2016/08/27.
 */
public class ReceiptListFragment extends Fragment implements ReceiptListContract.View, ReceiptListener {

    public static final int CAMERA_RESULT = 1;

    private MainActivityListener callback;
    private ReceiptListContract.Presenter presenter;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private int year;
    private int month;

    public static ReceiptListFragment newInstance(int year, int month) {
        ReceiptListFragment fragment = new ReceiptListFragment();
        Bundle args = new Bundle();
        args.putInt("year", year);
        args.putInt("month", month);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            callback = (MainActivityListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement MainActivityListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            year = getArguments().getInt("year", 2016);
            month = getArguments().getInt("month", 1);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, view);
        callback.setToolbarTitle("" + year + "/" + month);

        DbProvider dbProvider = new DbProvider(getActivity());

        presenter = new ReceiptListPresenter(dbProvider);
        presenter.attachView(this);

        recyclerView.addItemDecoration(new DividerItemDecoration(getContext()));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        if (getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            LinearLayout fabLayout = ButterKnife.findById(view, R.id.fab_layout);
            fabLayout.setOrientation(LinearLayout.HORIZONTAL);
        }

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.getReceipts(year, month);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.detachView();
        }
    }

    @OnClick(R.id.new_entry_fab)
    public void newEntryClicked() {
        presenter.newEntryClicked();
    }

    @OnClick(R.id.camera_fab)
    public void cameraFabClicked() {
        presenter.cameraFabClicked();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_RESULT && resultCode == Activity.RESULT_OK) {
            Calendar calendar = Calendar.getInstance();
            String id = data.getStringExtra("id");
            int year = data.getIntExtra("year", calendar.get(Calendar.YEAR));
            int month = data.getIntExtra("month", calendar.get(Calendar.MONTH));
            int day = data.getIntExtra("day", calendar.get(Calendar.DAY_OF_MONTH));
            int action = data.getIntExtra("action", ScannerActivity.ACTION_EDIT);
            presenter.cameraResult(id, year, month, day, action);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    // ReceiptListContract.View

    @Override
    public void displaySnackBar(CharSequence text) {
        CoordinatorLayout coordinatorLayout = ButterKnife.findById(getActivity(), R.id.coordinator_layout);
        Snackbar.make(coordinatorLayout, text, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void displayReceipts(List<Receipt> receiptList) {
        ReceiptAdapter adapter = new ReceiptAdapter(getContext(), receiptList, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void displayReceiptDialog(int id, String number, int year, int month, int day) {
        ReceiptDialog dialog = ReceiptDialog.newInstance(id, number, year, month, day);
        dialog.setTargetFragment(this, 0);
        dialog.show(getActivity().getSupportFragmentManager(), "Receipt Dialog");
    }

    @Override
    public void displayAddFragment() {
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.frame_layout, ReceiptAddEditFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void displayEditFragment(int id, String number, int year, int month, int day) {
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.frame_layout, ReceiptAddEditFragment.newInstance(id, number, year, month, day))
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    @Override
    public void launchCamera() {
        getActivity().startActivityForResult(new Intent(getActivity(), ScannerActivity.class), CAMERA_RESULT);
    }

    // ReceiptListener

    @Override
    public void onClick(int id, String number, int year, int month, int day) {
        presenter.onReceiptClick(id, number, year, month, day);
    }

    @Override
    public boolean onLongClick(int id, String number, int year, int month, int day) {
        return presenter.onReceiptLongClick(id, number, year, month, day);
    }

    @Override
    public void editClicked(int id, String number, int year, int month, int day) {
        presenter.editClicked(id, number, year, month, day);
    }

    @Override
    public void deleteClicked(int id) {
        if (presenter.deleteReceipt(id)) {
            presenter.getReceipts(year, month);
        }
    }
}
