package com.richolm.david.taiwanlottery.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import com.richolm.david.taiwanlottery.model.DateModel;
import com.richolm.david.taiwanlottery.model.Receipt;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kupo on 2016/08/27.
 */
public class DbProvider {
    private DatabaseHelper databaseHelper;

    public DbProvider(Context context) {
        databaseHelper = new DatabaseHelper(context);
    }

    public List<Receipt> getReceipts() {
        return getReceiptsFrom(0, 0);
    }

    public List<Receipt> getReceiptsFrom(int year, int month) {
        List<Receipt> receiptList = new ArrayList<>();
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String[] projection = {
                DatabaseHelper.ReceiptEntry._ID,
                DatabaseHelper.ReceiptEntry.COLUMN_NAME_NUMBER,
                DatabaseHelper.ReceiptEntry.COLUMN_NAME_YEAR,
                DatabaseHelper.ReceiptEntry.COLUMN_NAME_MONTH,
                DatabaseHelper.ReceiptEntry.COLUMN_NAME_DAY};
        String sortOrder = DatabaseHelper.ReceiptEntry.COLUMN_NAME_YEAR + " DESC, " +
                DatabaseHelper.ReceiptEntry.COLUMN_NAME_MONTH + " DESC, " +
                DatabaseHelper.ReceiptEntry.COLUMN_NAME_DAY + " DESC";
        String selection = DatabaseHelper.ReceiptEntry.COLUMN_NAME_YEAR + " = ? AND " +
                DatabaseHelper.ReceiptEntry.COLUMN_NAME_MONTH + " = ?";
        String[] selectionArgs = {Integer.toString(year), Integer.toString(month)};
        Cursor c;
        if (year == 0) {
            c = db.query(
                    DatabaseHelper.ReceiptEntry.TABLE_NAME,
                    projection,
                    null,
                    null,
                    null,
                    null,
                    sortOrder
            );
        } else {
            c = db.query(
                    DatabaseHelper.ReceiptEntry.TABLE_NAME,
                    projection,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    sortOrder
            );
        }
        while (c.moveToNext()) {
            receiptList.add(new Receipt(
                    c.getInt(c.getColumnIndex(DatabaseHelper.ReceiptEntry._ID)),
                    c.getString(c.getColumnIndex(DatabaseHelper.ReceiptEntry.COLUMN_NAME_NUMBER)),
                    c.getInt(c.getColumnIndex(DatabaseHelper.ReceiptEntry.COLUMN_NAME_YEAR)),
                    c.getInt(c.getColumnIndex(DatabaseHelper.ReceiptEntry.COLUMN_NAME_MONTH)),
                    c.getInt(c.getColumnIndex(DatabaseHelper.ReceiptEntry.COLUMN_NAME_DAY))
            ));
        }
        c.close();
        db.close();
        return receiptList;
    }

    public long addReceipt(String number, int year, int month, int day) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.ReceiptEntry.COLUMN_NAME_NUMBER, number);
        values.put(DatabaseHelper.ReceiptEntry.COLUMN_NAME_YEAR, year);
        values.put(DatabaseHelper.ReceiptEntry.COLUMN_NAME_MONTH, month);
        values.put(DatabaseHelper.ReceiptEntry.COLUMN_NAME_DAY, day);

        long result = db.insert(DatabaseHelper.ReceiptEntry.TABLE_NAME, null, values);
        db.close();
        return result;
    }

    public int delete(int id) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        String selection = DatabaseHelper.ReceiptEntry._ID + " = ?";
        String[] selectionArgs = {Integer.toString(id)};
        int deletedRows = db.delete(DatabaseHelper.ReceiptEntry.TABLE_NAME, selection, selectionArgs);
        db.close();
        return deletedRows;
    }

    public int editReceipt(int id, String number, int year, int month, int day) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.ReceiptEntry.COLUMN_NAME_NUMBER, number);
        values.put(DatabaseHelper.ReceiptEntry.COLUMN_NAME_YEAR, year);
        values.put(DatabaseHelper.ReceiptEntry.COLUMN_NAME_MONTH, month);
        values.put(DatabaseHelper.ReceiptEntry.COLUMN_NAME_DAY, day);

        String selection = DatabaseHelper.ReceiptEntry._ID + " = ?";
        String[] selectionArgs = {Integer.toString(id)};

        int result = 0;
        try {
            result = db.update(
                    DatabaseHelper.ReceiptEntry.TABLE_NAME,
                    values,
                    selection,
                    selectionArgs);
        } catch (SQLiteConstraintException e) {
            // TODO ?
        }
        db.close();
        return result;
    }

    public List<DateModel> getMonths() {
        List<DateModel> dateList = new ArrayList<>();
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String[] projection = {
                DatabaseHelper.ReceiptEntry.COLUMN_NAME_YEAR,
                DatabaseHelper.ReceiptEntry.COLUMN_NAME_MONTH
        };
        String sortOrder = DatabaseHelper.ReceiptEntry.COLUMN_NAME_YEAR + " DESC, " +
                DatabaseHelper.ReceiptEntry.COLUMN_NAME_MONTH + " DESC";
        Cursor c = db.query(
                true,
                DatabaseHelper.ReceiptEntry.TABLE_NAME,
                projection,
                null, // selection
                null, // selectionArgs
                null, // groupBy
                null, // having
                sortOrder,
                null // limit
        );
        while (c.moveToNext()) {
            dateList.add(new DateModel(
                    c.getInt(c.getColumnIndex(DatabaseHelper.ReceiptEntry.COLUMN_NAME_YEAR)),
                    c.getInt(c.getColumnIndex(DatabaseHelper.ReceiptEntry.COLUMN_NAME_MONTH))
            ));
        }
        c.close();
        db.close();
        return dateList;
    }

    public List<Receipt> checkForReceipt(String number, int digits, String year, int period) {
        List<Receipt> winningReceipts = new ArrayList<>();
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String[] projection = {
                DatabaseHelper.ReceiptEntry._ID,
                DatabaseHelper.ReceiptEntry.COLUMN_NAME_NUMBER,
                DatabaseHelper.ReceiptEntry.COLUMN_NAME_YEAR,
                DatabaseHelper.ReceiptEntry.COLUMN_NAME_MONTH,
                DatabaseHelper.ReceiptEntry.COLUMN_NAME_DAY};
        String sortOrder = DatabaseHelper.ReceiptEntry.COLUMN_NAME_YEAR + " DESC, " +
                DatabaseHelper.ReceiptEntry.COLUMN_NAME_MONTH + " DESC, " +
                DatabaseHelper.ReceiptEntry.COLUMN_NAME_DAY + " DESC";
        String searchNumber = number.substring(number.length() - digits);
        String selection = DatabaseHelper.ReceiptEntry.COLUMN_NAME_YEAR + " = ? AND " +
                "(" + DatabaseHelper.ReceiptEntry.COLUMN_NAME_MONTH + " = ? OR " +
                DatabaseHelper.ReceiptEntry.COLUMN_NAME_MONTH + " = ?) AND " +
                DatabaseHelper.ReceiptEntry.COLUMN_NAME_NUMBER + " LIKE ?";
        String[] selectionArgs = {
                year,
                Integer.toString(period * 2 + 1),
                Integer.toString(period * 2 + 2),
                "%" + searchNumber
        };
        Cursor c = db.query(
                DatabaseHelper.ReceiptEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
        while (c.moveToNext()) {
            winningReceipts.add(new Receipt(
                    c.getInt(c.getColumnIndex(DatabaseHelper.ReceiptEntry._ID)),
                    c.getString(c.getColumnIndex(DatabaseHelper.ReceiptEntry.COLUMN_NAME_NUMBER)),
                    c.getInt(c.getColumnIndex(DatabaseHelper.ReceiptEntry.COLUMN_NAME_YEAR)),
                    c.getInt(c.getColumnIndex(DatabaseHelper.ReceiptEntry.COLUMN_NAME_MONTH)),
                    c.getInt(c.getColumnIndex(DatabaseHelper.ReceiptEntry.COLUMN_NAME_DAY))
            ));
        }
        c.close();
        db.close();
        return winningReceipts;
    }
}
