package com.richolm.david.taiwanlottery.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;

import com.richolm.david.taiwanlottery.R;
import com.richolm.david.taiwanlottery.contracts.ReceiptEditContract;
import com.richolm.david.taiwanlottery.database.DbProvider;
import com.richolm.david.taiwanlottery.listeners.MainActivityListener;
import com.richolm.david.taiwanlottery.presenters.ReceiptEditPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kupo on 2016/08/29.
 */
public class ReceiptAddEditFragment extends Fragment implements ReceiptEditContract.View {

    private MainActivityListener callback;
    private ReceiptEditContract.Presenter presenter;


    @BindView(R.id.receipt_edittext)
    EditText numberView;
    @BindView(R.id.date_picker)
    DatePicker datePicker;

    private int id = -1;
    private String number;
    private int year;
    private int month;
    private int day;

    public static ReceiptAddEditFragment newInstance() {
        return new ReceiptAddEditFragment();
    }

    public static ReceiptAddEditFragment newInstance(int id, String number, int year, int month, int day) {
        ReceiptAddEditFragment fragment = new ReceiptAddEditFragment();
        Bundle args = new Bundle();
        args.putInt("id", id);
        args.putString("number", number);
        args.putInt("year", year);
        args.putInt("month", month);
        args.putInt("day", day);
        args.putBoolean("camera", true);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            callback = (MainActivityListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().getClass().getSimpleName() + " must implement MainActivityListener!");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getInt("id", -1);
            number = getArguments().getString("number");
            year = getArguments().getInt("year");
            month = getArguments().getInt("month");
            day = getArguments().getInt("day");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_receipt_addedit, container, false);
        ButterKnife.bind(this, view);
        if (number == null) {
            callback.setToolbarTitle(R.string.add_receipt);
        } else {
            callback.setToolbarTitle(number);
            numberView.setText(number);
            datePicker.updateDate(year, month - 1, day);
        }
        setHasOptionsMenu(true);

        DbProvider provider = new DbProvider(getActivity());
        presenter = new ReceiptEditPresenter(provider);
        presenter.attachView(this);

        numberView.requestFocus();
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.receipt_addedit, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.submit_receipt:
                if (!presenter.checkIdSyntax(numberView.getText().toString())) {
                    return true;
                }
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

                if (!presenter.submitClicked(id, numberView.getText().toString(), datePicker.getYear(), datePicker.getMonth() + 1, datePicker.getDayOfMonth())) {
                    Snackbar.make(getView(), R.string.receipt_error, Snackbar.LENGTH_SHORT).show();
                    return true;
                }
                getActivity()
                        .getSupportFragmentManager()
                        .popBackStack();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void invalidId() {
        Animation shake = AnimationUtils.loadAnimation(getContext(), R.anim.shake);
        numberView.setAnimation(shake);
        numberView.setError(getResources().getString(R.string.invalid_id));
    }
}
