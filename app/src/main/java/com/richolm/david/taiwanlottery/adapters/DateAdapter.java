package com.richolm.david.taiwanlottery.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.richolm.david.taiwanlottery.R;
import com.richolm.david.taiwanlottery.listeners.DateListener;
import com.richolm.david.taiwanlottery.model.DateModel;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by Kupo on 2016/09/18.
 */
public class DateAdapter extends RecyclerView.Adapter<DateAdapter.DateHolder> {

    private final String TAG = getClass().getSimpleName();

    private List<DateModel> dateList;
    private DateListener onClickListener;

    public DateAdapter(List<DateModel> dateList, DateListener onClickListener) {
        this.dateList = dateList;
        this.onClickListener = onClickListener;
    }

    public class DateHolder extends RecyclerView.ViewHolder {
        int year;
        int month;
        TextView dateView;

        public DateHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onClickListener == null) {
                        Log.e(TAG, "OnClickListener not set");
                        return;
                    }
                    onClickListener.onClick(year, month);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (onClickListener == null) {
                        Log.e(TAG, "OnClickListener not set");
                        return false;
                    }
                    return onClickListener.onLongClick(year, month);
                }
            });

            dateView = ButterKnife.findById(itemView, R.id.date_view);
        }
    }

    @Override
    public DateHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_date, parent, false);
        return new DateHolder(view);
    }

    @Override
    public void onBindViewHolder(DateHolder holder, int position) {
        holder.year = dateList.get(position).getYear();
        holder.month = dateList.get(position).getMonth();
        String date = holder.year + "/" + holder.month;
        holder.dateView.setText(date);
    }

    @Override
    public int getItemCount() {
        return dateList.size();
    }
}
