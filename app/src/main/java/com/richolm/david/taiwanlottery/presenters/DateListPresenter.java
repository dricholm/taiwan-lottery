package com.richolm.david.taiwanlottery.presenters;

import com.richolm.david.taiwanlottery.contracts.DateListContract;
import com.richolm.david.taiwanlottery.database.DbProvider;

import java.lang.ref.WeakReference;

/**
 * Created by Kupo on 2016/09/18.
 */
public class DateListPresenter implements DateListContract.Presenter {

    private WeakReference<DateListContract.View> view;
    private DbProvider dbProvider;

    public DateListPresenter(DbProvider dbProvider){
        this.dbProvider = dbProvider;
    }

    @Override
    public void attachView(DateListContract.View view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        if (!isViewAttached()){
            view = null;
        }
    }

    @Override
    public boolean isViewAttached() {
        return view != null;
    }

    @Override
    public void getMonths() {
        if (!isViewAttached()){
            return;
        }
        view.get().displayMonths(dbProvider.getMonths());
    }

    @Override
    public void newEntryClicked() {
        if (!isViewAttached()) {
            return;
        }
        view.get().displayAddFragment();
    }

    @Override
    public void cameraFabClicked() {
        if (!isViewAttached()) {
            return;
        }
        view.get().launchCamera();
    }

    @Override
    public void dateClicked(int year, int month) {
        if (!isViewAttached()){
            return;
        }
        view.get().openReceiptList(year, month);
    }
}
