package com.richolm.david.taiwanlottery.listeners;

/**
 * Created by Kupo on 2016/08/27.
 */
public interface MainActivityListener {
    void setToolbarTitle(int resId);

    void setToolbarTitle(String title);

    void backButtonEnabled(boolean enabled);
}
