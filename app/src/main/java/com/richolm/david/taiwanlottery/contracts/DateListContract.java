package com.richolm.david.taiwanlottery.contracts;

import com.richolm.david.taiwanlottery.model.DateModel;

import java.util.List;

/**
 * Created by Kupo on 2016/09/18.
 */
public abstract class DateListContract {
    public interface View {
        void displayMonths(List<DateModel> dateList);

        void openReceiptList(int year, int month);

        void displayAddFragment();

        void launchCamera();
    }

    public interface Presenter {
        void attachView(DateListContract.View view);

        void detachView();

        boolean isViewAttached();

        void getMonths();

        void newEntryClicked();

        void cameraFabClicked();

        void dateClicked(int year, int month);
    }
}
