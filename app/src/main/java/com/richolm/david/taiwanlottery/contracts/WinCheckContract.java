package com.richolm.david.taiwanlottery.contracts;

import com.richolm.david.taiwanlottery.model.Receipt;

import java.util.List;

/**
 * Created by Kupo on 2016/09/21.
 */
public abstract class WinCheckContract {
    public interface View{
        void displayWinnings(List<Receipt> receiptList, int winType);

        void displayWinningsPanel(int totalWinnings);

        void displaySnackBar(int resId);
    }

    public interface Presenter{
        void attachView(WinCheckContract.View view);

        void detachView();

        boolean isViewAttached();

        void manualCheckClicked(String specialPrize, String grandPrize, String[] prizes, String[] sixthPrizes, String year, int period);
    }
}
