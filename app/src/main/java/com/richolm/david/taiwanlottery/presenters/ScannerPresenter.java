package com.richolm.david.taiwanlottery.presenters;

import com.richolm.david.taiwanlottery.contracts.ScannerContract;
import com.richolm.david.taiwanlottery.database.DbProvider;

import java.lang.ref.WeakReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Kupo on 2016/09/03.
 */
public class ScannerPresenter implements ScannerContract.Presenter {

    private WeakReference<ScannerContract.View> view;
    private DbProvider dbProvider;

    public ScannerPresenter(DbProvider dbProvider) {
        this.dbProvider = dbProvider;
    }

    @Override
    public void attachView(ScannerContract.View view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        if (isViewAttached()) {
            view = null;
        }
    }

    @Override
    public boolean isViewAttached() {
        return view != null;
    }

    @Override
    public void retryClicked() {
        view.get().hideDoneView();
        view.get().displayInstructions();
        view.get().resumeCamera();
    }

    @Override
    public void addClicked() {

    }

    @Override
    public void gotResult(String text) {
        // TODO: Check for first 2 chars, MOS
        Pattern pattern = Pattern.compile("[A-Z]{2}[0-9]{15}");
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            String id = text.substring(2, 10);
            int year = Integer.parseInt(text.substring(10, 13)) + 1911;
            int month = Integer.parseInt(text.substring(13, 15));
            int day = Integer.parseInt(text.substring(15, 17));
            view.get().displaySuccess();
            view.get().displayDoneView(id, year, month, day);
        } else {
            view.get().displayInvalidFormat();
            view.get().resumeCamera();
        }
    }
}
