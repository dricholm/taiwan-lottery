package com.richolm.david.taiwanlottery.presenters;

import com.richolm.david.taiwanlottery.contracts.ReceiptEditContract;
import com.richolm.david.taiwanlottery.database.DbProvider;

import java.lang.ref.WeakReference;

/**
 * Created by Kupo on 2016/09/03.
 */
public class ReceiptEditPresenter implements ReceiptEditContract.Presenter {

    private WeakReference<ReceiptEditContract.View> view;
    private DbProvider dbProvider;

    public ReceiptEditPresenter(DbProvider dbProvider) {
        this.dbProvider = dbProvider;
    }

    @Override
    public void attachView(ReceiptEditContract.View view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        if (isViewAttached()) {
            view = null;
        }
    }

    @Override
    public boolean isViewAttached() {
        return view != null;
    }

    @Override
    public boolean checkIdSyntax(String id) {
        if (id.length() != 8) {
            view.get().invalidId();
            return false;
        }else{
            return true;
        }
    }

    @Override
    public boolean submitClicked(int id, String number, int year, int month, int day) {
        if (id == -1) {
            return dbProvider.addReceipt(number, year, month, day) >= 0;
        } else {
            return dbProvider.editReceipt(id, number, year, month, day) > 0;
        }
    }
}
