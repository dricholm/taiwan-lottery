package com.richolm.david.taiwanlottery.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.richolm.david.taiwanlottery.R;
import com.richolm.david.taiwanlottery.fragments.DateListFragment;
import com.richolm.david.taiwanlottery.fragments.WinCheckFragment;
import com.richolm.david.taiwanlottery.fragments.ReceiptListFragment;
import com.richolm.david.taiwanlottery.listeners.MainActivityListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import icepick.Icepick;
import icepick.State;

public class MainActivity extends AppCompatActivity implements MainActivityListener {

    private final String TAG = getClass().getSimpleName();

    @BindView(R.id.navigation_view)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    private ActionBarDrawerToggle actionBarDrawerToggle;

    @State
    boolean backButtonEnabled = true;
    @State
    int checkedMenuItemId = R.id.nav_receipt_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (sharedPreferences.getBoolean(getString(R.string.settings_night), false)) {
            Log.d(TAG, "Entering night mode");
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        } else {
            Log.d(TAG, "Entering day mode");
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        setContentView(R.layout.activity_receipt_list);
        ButterKnife.bind(this);
        Icepick.restoreInstanceState(this, savedInstanceState);

        Toolbar toolbar = ButterKnife.findById(this, R.id.toolbar);
        setSupportActionBar(toolbar);

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        Fragment fragment = null;
                        switch (menuItem.getItemId()) {
                            case R.id.nav_receipt_list:
                                fragment = DateListFragment.newInstance();
                                break;
                            case R.id.nav_win_check:
                                fragment = WinCheckFragment.newInstance();
                                break;
                            case R.id.nav_settings:
                                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                                break;
                        }
                        if (fragment != null) {
                            checkedMenuItemId = menuItem.getItemId();
                            getSupportFragmentManager()
                                    .beginTransaction()
                                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                                    .replace(R.id.frame_layout, fragment)
                                    .commit();
                        }
                        drawerLayout.closeDrawers();
                        return true;
                    }
                });

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        actionBarDrawerToggle.syncState();

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);

        if (savedInstanceState == null) {
            Log.d(TAG, "New creation of MainActivity");
            getSupportFragmentManager().beginTransaction().add(R.id.frame_layout, DateListFragment.newInstance()).commit();
        } else {
            Log.d(TAG, "MainActivity restored");
            for (int i = 0; i < navigationView.getMenu().size(); i++) {
                navigationView.getMenu().getItem(i).setChecked(false);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.getMenu().findItem(checkedMenuItemId).setChecked(true);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
            return;
        }
        if (backButtonEnabled) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ReceiptListFragment.CAMERA_RESULT && resultCode == Activity.RESULT_OK) {
            getSupportFragmentManager().findFragmentById(R.id.frame_layout).onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    // MainActivityListener

    @Override
    public void setToolbarTitle(int resId) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getResources().getString(resId));
        }
    }

    @Override
    public void setToolbarTitle(String title) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    @Override
    public void backButtonEnabled(boolean enabled) {
        backButtonEnabled = enabled;
    }
}
