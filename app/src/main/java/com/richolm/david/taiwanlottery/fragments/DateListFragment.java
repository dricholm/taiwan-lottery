package com.richolm.david.taiwanlottery.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.richolm.david.taiwanlottery.R;
import com.richolm.david.taiwanlottery.activities.ScannerActivity;
import com.richolm.david.taiwanlottery.adapters.DateAdapter;
import com.richolm.david.taiwanlottery.contracts.DateListContract;
import com.richolm.david.taiwanlottery.database.DbProvider;
import com.richolm.david.taiwanlottery.decorations.DividerItemDecoration;
import com.richolm.david.taiwanlottery.listeners.DateListener;
import com.richolm.david.taiwanlottery.listeners.MainActivityListener;
import com.richolm.david.taiwanlottery.model.DateModel;
import com.richolm.david.taiwanlottery.presenters.DateListPresenter;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Kupo on 2016/09/18.
 */
public class DateListFragment extends Fragment implements DateListContract.View, DateListener {

    public static final int CAMERA_RESULT = 1;

    private MainActivityListener callback;
    private DateListContract.Presenter presenter;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    public static DateListFragment newInstance() {
        return new DateListFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            callback = (MainActivityListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement MainActivityListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, view);
        callback.setToolbarTitle(R.string.receipt_list);

        DbProvider dbProvider = new DbProvider(getActivity());

        presenter = new DateListPresenter(dbProvider);
        presenter.attachView(this);

        recyclerView.addItemDecoration(new DividerItemDecoration(getContext()));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        if (getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            LinearLayout fabLayout = ButterKnife.findById(view, R.id.fab_layout);
            fabLayout.setOrientation(LinearLayout.HORIZONTAL);
        }

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.getMonths();
    }

    @OnClick(R.id.new_entry_fab)
    public void newEntryClicked() {
        presenter.newEntryClicked();
    }

    @OnClick(R.id.camera_fab)
    public void cameraFabClicked() {
        presenter.cameraFabClicked();
    }

    // DateListContract.View

    @Override
    public void displayMonths(List<DateModel> dateList) {
        DateAdapter dateAdapter = new DateAdapter(dateList, this);
        recyclerView.setAdapter(dateAdapter);
    }

    @Override
    public void openReceiptList(int year, int month) {
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.frame_layout, ReceiptListFragment.newInstance(year, month))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void displayAddFragment() {
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.frame_layout, ReceiptAddEditFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void launchCamera() {
        getActivity().startActivityForResult(new Intent(getActivity(), ScannerActivity.class), CAMERA_RESULT);
    }

    // DateListener

    @Override
    public void onClick(int year, int month) {
        presenter.dateClicked(year, month);
    }

    @Override
    public boolean onLongClick(int year, int month) {
        return false;
    }

    @Override
    public void deleteClicked(int year, int month) {

    }
}
