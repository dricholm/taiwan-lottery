package com.richolm.david.taiwanlottery.contracts;

/**
 * Created by Kupo on 2016/09/03.
 */
public abstract class ReceiptEditContract {
    public interface View {
        void invalidId();
    }

    public interface Presenter {
        void attachView(ReceiptEditContract.View view);

        void detachView();

        boolean isViewAttached();

        boolean checkIdSyntax(String id);

        boolean submitClicked(int id, String number, int year, int month, int day);
    }
}
