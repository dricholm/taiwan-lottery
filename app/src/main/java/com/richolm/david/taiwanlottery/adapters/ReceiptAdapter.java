package com.richolm.david.taiwanlottery.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.richolm.david.taiwanlottery.R;
import com.richolm.david.taiwanlottery.listeners.ReceiptListener;
import com.richolm.david.taiwanlottery.model.Receipt;

import java.util.Calendar;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by Kupo on 2016/08/21.
 */
public class ReceiptAdapter extends RecyclerView.Adapter<ReceiptAdapter.ReceiptHolder> {

    private final String TAG = getClass().getSimpleName();

    private Context context;

    private List<Receipt> receiptList;
    private ReceiptListener onClickListener;

    public ReceiptAdapter(Context context, List<Receipt> receiptList, ReceiptListener onClickListener) {
        this.context = context;
        this.receiptList = receiptList;
        this.onClickListener = onClickListener;
    }

    public class ReceiptHolder extends RecyclerView.ViewHolder {
        TextView numberView;
        TextView dateView;
        int id;
        int year;
        int month;
        int day;

        public ReceiptHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onClickListener == null) {
                        Log.e(TAG, "OnClickListener not set");
                        return;
                    }
                    onClickListener.onClick(id, numberView.getText().toString(), year, month, day);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (onClickListener == null) {
                        Log.e(TAG, "OnClickListener not set");
                        return false;
                    }
                    return onClickListener.onLongClick(id, numberView.getText().toString(), year, month, day);
                }
            });

            numberView = ButterKnife.findById(itemView, R.id.receipt_title);
            dateView = ButterKnife.findById(itemView, R.id.receipt_date);
        }
    }

    @Override
    public ReceiptHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_receipt, parent, false);
        return new ReceiptHolder(view);
    }

    @Override
    public void onBindViewHolder(ReceiptHolder holder, int position) {
        Receipt receipt = receiptList.get(position);
        holder.numberView.setText(receipt.getNumber());
        holder.id = receipt.getId();
        holder.year = receipt.getYear();
        holder.month = receipt.getMonth();
        holder.day = receipt.getDay();
        Calendar calendar = Calendar.getInstance();
        calendar.set(receipt.getYear(), receipt.getMonth() - 1, receipt.getDay());
        String date = DateUtils.formatDateTime(context, calendar.getTimeInMillis(), DateUtils.FORMAT_SHOW_YEAR);
        String day = DateUtils.formatDateTime(context, calendar.getTimeInMillis(), DateUtils.FORMAT_SHOW_WEEKDAY);
        String text = date + "\n" + day;
        holder.dateView.setText(text);
    }

    @Override
    public int getItemCount() {
        return receiptList.size();
    }
}
