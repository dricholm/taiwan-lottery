package com.richolm.david.taiwanlottery.presenters;


import com.richolm.david.taiwanlottery.activities.ScannerActivity;
import com.richolm.david.taiwanlottery.contracts.ReceiptListContract;
import com.richolm.david.taiwanlottery.database.DbProvider;

import java.lang.ref.WeakReference;

/**
 * Created by Kupo on 2016/08/21.
 */
public class ReceiptListPresenter implements ReceiptListContract.Presenter {

    private WeakReference<ReceiptListContract.View> view;
    private DbProvider dbProvider;

    public ReceiptListPresenter(DbProvider dbProvider) {
        this.dbProvider = dbProvider;
    }

    @Override
    public void attachView(ReceiptListContract.View view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        view = null;
    }

    @Override
    public boolean isViewAttached() {
        return view != null;
    }

    @Override
    public void getReceipts(int year, int month) {
        if (!isViewAttached()) {
            return;
        }
        view.get().displayReceipts(dbProvider.getReceiptsFrom(year, month));
    }

    @Override
    public void newEntryClicked() {
        if (!isViewAttached()) {
            return;
        }
        view.get().displayAddFragment();
    }

    @Override
    public void cameraFabClicked() {
        if (!isViewAttached()) {
            return;
        }
        view.get().launchCamera();
    }

    @Override
    public void onReceiptClick(int id, String number, int year, int month, int day) {
        if (!isViewAttached()) {
            return;
        }
        view.get().displaySnackBar("Clicked " + number);
    }

    @Override
    public boolean onReceiptLongClick(int id, String number, int year, int month, int day) {
        if (!isViewAttached()) {
            return false;
        }
        view.get().displayReceiptDialog(id, number, year, month, day);
        return true;
    }

    @Override
    public void editClicked(int id, String number, int year, int month, int day) {
        if (!isViewAttached()) {
            return;
        }
        view.get().displayEditFragment(id, number, year, month, day);
    }

    @Override
    public boolean deleteReceipt(int id) {
        return dbProvider.delete(id) > 0;
    }

    @Override
    public void cameraResult(String number, int year, int month, int day, int action) {
        switch (action) {
            case ScannerActivity.ACTION_ADD:
                dbProvider.addReceipt(number, year, month, day);
                break;
            case ScannerActivity.ACTION_EDIT:
                view.get().displayEditFragment(-1, number, year, month, day);
                break;
        }
    }
}
