package com.richolm.david.taiwanlottery.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.richolm.david.taiwanlottery.R;
import com.richolm.david.taiwanlottery.contracts.WinCheckContract;
import com.richolm.david.taiwanlottery.database.DbProvider;
import com.richolm.david.taiwanlottery.listeners.MainActivityListener;
import com.richolm.david.taiwanlottery.model.Receipt;
import com.richolm.david.taiwanlottery.presenters.WinCheckPresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import icepick.Icepick;
import icepick.State;

/**
 * Created by Kupo on 2016/09/17.
 */
public class WinCheckFragment extends Fragment implements WinCheckContract.View {

    public static final int SPECIAL_PRIZE = 10000000;
    public static final int GRAND_PRIZE = 2000000;
    public static final int REGULAR_PRIZE = 200000;
    public static final int SECOND_PRIZE = 40000;
    public static final int THIRD_PRIZE = 10000;
    public static final int FOURTH_PRIZE = 4000;
    public static final int FIFTH_PRIZE = 1000;
    public static final int SIXTH_PRIZE = 200;

    private MainActivityListener callback;
    private WinCheckContract.Presenter presenter;

    public enum State {BASE, WINNINGS}

    ;

    @icepick.State
    State state = State.BASE;

    @BindView(R.id.manual_check_panel)
    LinearLayout manualCheckPanelView;
    @BindView(R.id.results_panel)
    LinearLayout resultsPanelView;
    @BindView(R.id.winning_receipts)
    LinearLayout winningReceiptsView;
    @BindView(R.id.result_money)
    TextView resultMoneyView;

    public static WinCheckFragment newInstance() {
        return new WinCheckFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            callback = (MainActivityListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement MainActivityListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_win_check, container, false);
        ButterKnife.bind(this, view);
        Icepick.restoreInstanceState(this, savedInstanceState);
        callback.setToolbarTitle(R.string.check_win);

        DbProvider dbProvider = new DbProvider(getContext());
        presenter = new WinCheckPresenter(dbProvider);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.attachView(this);

        if (getView() != null) {
            getView().setFocusableInTouchMode(true);
            getView().requestFocus();
            getView().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                        if (state != State.BASE) {
                            manualCheckPanelView.setVisibility(View.VISIBLE);
                            resultsPanelView.setVisibility(View.INVISIBLE);
                            winningReceiptsView.removeAllViews();
                            state = State.BASE;
                            return true;
                        }
                    }
                    return false;
                }
            });
        }

        callback.backButtonEnabled(state == State.BASE);
    }

    @Override
    public void onPause() {
        super.onPause();
        callback.backButtonEnabled(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @OnClick(R.id.check_button)
    public void checkClicked() {
        if (getView() == null) {
            return;
        }
        boolean validEntries = false;

        EditText specialPrize = ButterKnife.findById(getView(), R.id.special_prize);
        if (!specialPrize.getText().toString().equals("") && specialPrize.getText().toString().length() == 8) {
            validEntries = true;
        } else {
            specialPrize.setError(getString(R.string.invalid_number));
        }

        EditText grandPrizeView = ButterKnife.findById(getView(), R.id.grand_prize);
        if (!grandPrizeView.getText().toString().equals("") && grandPrizeView.getText().toString().length() == 8) {
            validEntries = true;
        } else {
            grandPrizeView.setError(getString(R.string.invalid_number));
        }

        EditText prizeOneView = ButterKnife.findById(getView(), R.id.prize_1);
        if (!prizeOneView.getText().toString().equals("") && prizeOneView.getText().toString().length() == 8) {
            validEntries = true;
        } else {
            prizeOneView.setError(getString(R.string.invalid_number));
        }

        EditText prizeTwoView = ButterKnife.findById(getView(), R.id.prize_2);
        if (!prizeTwoView.getText().toString().equals("") && prizeTwoView.getText().toString().length() == 8) {
            validEntries = true;
        } else {
            prizeTwoView.setError(getString(R.string.invalid_number));
        }

        EditText prizeThree = ButterKnife.findById(getView(), R.id.prize_3);
        if (!prizeThree.getText().toString().equals("") && prizeThree.getText().toString().length() == 8) {
            validEntries = true;
        } else {
            prizeThree.setError(getString(R.string.invalid_number));
        }

        EditText sixthPrize = ButterKnife.findById(getView(), R.id.sixth_prize);
        String[] sixthPrizes;
        if (!sixthPrize.getText().toString().equals("") && sixthPrize.getText().toString().length() == 3) {
            validEntries = true;
            sixthPrizes = new String[]{sixthPrize.getText().toString()};
        } else {
            sixthPrizes = sixthPrize.getText().toString().split(",");
            boolean validSixthPrize = false;
            for (String s : sixthPrizes) {
                if (s.length() == 3) {
                    validSixthPrize = true;
                    break;
                }
            }
            if (validSixthPrize) {
                validEntries = true;
            } else {
                sixthPrize.setError(getString(R.string.invalid_number_three));
            }
        }

        if (!validEntries) {
            return;
        }

        Spinner periodSpinner = ButterKnife.findById(getView(), R.id.period_spinner);
        presenter.manualCheckClicked(
                specialPrize.getText().toString(),
                grandPrizeView.getText().toString(),
                new String[]{prizeOneView.getText().toString(),
                        prizeTwoView.getText().toString(),
                        prizeThree.getText().toString()},
                sixthPrizes,
                "2016",
                periodSpinner.getSelectedItemPosition()
        );
    }

    // WinCheckContract.View

    @Override
    public void displayWinnings(List<Receipt> receiptList, int winType) {
        if (receiptList.isEmpty()) {
            return;
        }
        TextView textView = new TextView(getContext());
        switch (winType) {
            case SPECIAL_PRIZE:
                textView.setText(R.string.special_prize);
                break;
            case GRAND_PRIZE:
                textView.setText(R.string.grand_prize);
                break;
            case REGULAR_PRIZE:
                textView.setText(R.string.regular_prize);
                break;
            case SECOND_PRIZE:
                textView.setText(R.string.second_prize);
                break;
            case THIRD_PRIZE:
                textView.setText(R.string.third_prize);
                break;
            case FOURTH_PRIZE:
                textView.setText(R.string.fourth_prize);
                break;
            case FIFTH_PRIZE:
                textView.setText(R.string.fifth_prize);
                break;
            case SIXTH_PRIZE:
                textView.setText(R.string.sixth_prize);
                break;
        }
        textView.setPadding(5, 10, 5, 5);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18f);
        winningReceiptsView.addView(textView);
        for (Receipt receipt : receiptList) {
            textView = new TextView(getContext());
            textView.setPadding(20, 5, 5, 5);
            textView.setText(receipt.getNumber());
            winningReceiptsView.addView(textView);
        }
    }

    @Override
    public void displayWinningsPanel(int totalWinnings) {
        callback.backButtonEnabled(false);
        state = State.WINNINGS;

        resultMoneyView.setText(getContext().getString(R.string.total_winnings, totalWinnings));

        manualCheckPanelView.setVisibility(View.INVISIBLE);
        resultsPanelView.setVisibility(View.VISIBLE);
    }

    @Override
    public void displaySnackBar(int resId) {
        if (getView() != null) {
            Snackbar.make(getView(), resId, Snackbar.LENGTH_LONG).show();
        }
    }
}
