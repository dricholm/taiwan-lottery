package com.richolm.david.taiwanlottery.contracts;

import com.richolm.david.taiwanlottery.model.Receipt;

import java.util.List;

/**
 * Created by Kupo on 2016/08/21.
 */
public abstract class ReceiptListContract {
    public interface View {
        void displaySnackBar(CharSequence text);

        void displayReceipts(List<Receipt> receiptList);

        void displayReceiptDialog(int id, String number, int year, int month, int day);

        void displayAddFragment();

        void displayEditFragment(int id, String number, int year, int month, int day);

        void launchCamera();
    }

    public interface Presenter {
        void attachView(ReceiptListContract.View view);

        void detachView();

        boolean isViewAttached();

        void getReceipts(int year, int month);

        void newEntryClicked();

        void cameraFabClicked();

        void onReceiptClick(int id, String number, int year, int month, int day);

        boolean onReceiptLongClick(int id, String number, int year, int month, int day);

        void editClicked(int id, String number, int year, int month, int day);

        boolean deleteReceipt(int id);

        void cameraResult(String number, int year, int month, int day, int action);
    }
}
