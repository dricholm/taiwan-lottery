package com.richolm.david.taiwanlottery.model;


/**
 * Created by Kupo on 2016/08/21.
 */
public class Receipt {
    private int id;
    private String number;
    private int year;
    private int month;
    private int day;

    public Receipt(int id, String number, int year, int month, int day) {
        this.id = id;
        this.number = number;
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public int getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }
}
