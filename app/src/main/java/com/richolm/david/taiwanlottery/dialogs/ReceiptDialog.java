package com.richolm.david.taiwanlottery.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.richolm.david.taiwanlottery.R;
import com.richolm.david.taiwanlottery.listeners.ReceiptListener;

/**
 * Created by Kupo on 2016/08/28.
 */
public class ReceiptDialog extends DialogFragment {

    private int id;
    private String number;
    private int year;
    private int month;
    private int day;

    private ReceiptListener callback;

    public static ReceiptDialog newInstance(int id, String number, int year, int month, int day) {
        ReceiptDialog fragment = new ReceiptDialog();
        Bundle args = new Bundle();
        args.putInt("id", id);
        args.putString("number", number);
        args.putInt("year", year);
        args.putInt("month", month);
        args.putInt("day", day);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            callback = (ReceiptListener) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement ReceiptListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            id = bundle.getInt("id");
            number = bundle.getString("number");
            year = bundle.getInt("year");
            month = bundle.getInt("month");
            day = bundle.getInt("day");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(number);
        builder.setItems(R.array.select_dialog_items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int menuChoice) {
                switch (menuChoice) {
                    case 0:
                        callback.editClicked(id, number, year, month, day);
                        break;
                    case 1:
                        callback.deleteClicked(id);
                        break;
                }
            }
        });
        return builder.create();
    }
}
