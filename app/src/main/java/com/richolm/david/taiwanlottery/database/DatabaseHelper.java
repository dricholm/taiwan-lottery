package com.richolm.david.taiwanlottery.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by Kupo on 2016/08/28.
 */
public final class DatabaseHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Receipts.db";

    public static class ReceiptEntry implements BaseColumns {
        public static final String TABLE_NAME = "receipts";
        public static final String COLUMN_NAME_NUMBER = "number";
        public static final String COLUMN_NAME_YEAR = "year";
        public static final String COLUMN_NAME_MONTH = "month";
        public static final String COLUMN_NAME_DAY = "day";

        private static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY, " +
                        COLUMN_NAME_NUMBER + " VARCHAR(8) NOT NULL, " +
                        COLUMN_NAME_YEAR + " INTEGER NOT NULL, " +
                        COLUMN_NAME_MONTH + " INTEGER NOT NULL, " +
                        COLUMN_NAME_DAY + " INTEGER NOT NULL, " +
                        "CONSTRAINT receipt_unique UNIQUE (" + COLUMN_NAME_NUMBER + ", " + COLUMN_NAME_YEAR + ", " + COLUMN_NAME_MONTH + ", " + COLUMN_NAME_DAY + "))";

        private static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(ReceiptEntry.SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL(ReceiptEntry.SQL_DELETE_ENTRIES);
        onCreate(sqLiteDatabase);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }


}
