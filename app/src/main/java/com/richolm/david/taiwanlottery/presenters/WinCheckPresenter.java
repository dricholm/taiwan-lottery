package com.richolm.david.taiwanlottery.presenters;

import android.util.Log;

import com.richolm.david.taiwanlottery.R;
import com.richolm.david.taiwanlottery.contracts.WinCheckContract;
import com.richolm.david.taiwanlottery.database.DbProvider;
import com.richolm.david.taiwanlottery.fragments.WinCheckFragment;
import com.richolm.david.taiwanlottery.model.Receipt;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kupo on 2016/09/21.
 */
public class WinCheckPresenter implements WinCheckContract.Presenter {

    private final String TAG = getClass().getSimpleName();

    private WeakReference<WinCheckContract.View> view;

    private DbProvider dbProvider;

    public WinCheckPresenter(DbProvider dbProvider) {
        this.dbProvider = dbProvider;
    }

    @Override
    public void attachView(WinCheckContract.View view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        if (isViewAttached()) {
            view = null;
        }
    }

    @Override
    public boolean isViewAttached() {
        return view != null;
    }

    @Override
    public void manualCheckClicked(String specialPrize, String grandPrize, String[] prizes, String sixthPrizes[], String year, int period) {
        if (!isViewAttached()) {
            return;
        }
        int totalWinnings = 0;
        List<Receipt> winningList = new ArrayList<>();
        boolean win = false;
        if (!specialPrize.equals("")) {
            winningList = dbProvider.checkForReceipt(specialPrize, 8, year, period);
            if (!winningList.isEmpty()) {
                win = true;
            }
        }
        view.get().displayWinnings(winningList, WinCheckFragment.SPECIAL_PRIZE);
        totalWinnings += WinCheckFragment.SPECIAL_PRIZE * winningList.size();
        winningList.clear();

        if (!grandPrize.equals("")) {
            winningList = dbProvider.checkForReceipt(grandPrize, 8, year, period);
            if (!win && !winningList.isEmpty()) {
                win = true;
            }
        }
        view.get().displayWinnings(winningList, WinCheckFragment.GRAND_PRIZE);
        totalWinnings += WinCheckFragment.GRAND_PRIZE * winningList.size();
        winningList.clear();

        List<Receipt> secondList = new ArrayList<>();
        List<Receipt> thirdList = new ArrayList<>();
        List<Receipt> fourthList = new ArrayList<>();
        List<Receipt> fifthList = new ArrayList<>();
        List<Receipt> sixthList = new ArrayList<>();
        for (String prize : prizes) {
            if (!prize.equals("")) {
                int digits = 7;
                do {
                    winningList = dbProvider.checkForReceipt(prize, digits, year, period);
                    digits--;
                } while (winningList.isEmpty() && digits > 2);
                if (!win && !winningList.isEmpty()) {
                    win = true;
                }
                switch (digits) {
                    case 6:
                        secondList.addAll(winningList);
                        break;
                    case 5:
                        thirdList.addAll(winningList);
                        break;
                    case 4:
                        fourthList.addAll(winningList);
                        break;
                    case 3:
                        fifthList.addAll(winningList);
                        break;
                    case 2:
                        sixthList.addAll(winningList);
                        break;
                }
                winningList.clear();
            }
        }

        for (String s : sixthPrizes) {
            if (!s.equals("")) {
                sixthList.addAll(dbProvider.checkForReceipt(s, 3, year, period));
                if (!win && !sixthList.isEmpty()) {
                    win = true;
                }
            }
        }
        view.get().displayWinnings(secondList, WinCheckFragment.SECOND_PRIZE);
        totalWinnings += WinCheckFragment.SECOND_PRIZE * secondList.size();
        view.get().displayWinnings(thirdList, WinCheckFragment.THIRD_PRIZE);
        totalWinnings += WinCheckFragment.THIRD_PRIZE * thirdList.size();
        view.get().displayWinnings(fourthList, WinCheckFragment.FOURTH_PRIZE);
        totalWinnings += WinCheckFragment.FOURTH_PRIZE * fourthList.size();
        view.get().displayWinnings(fifthList, WinCheckFragment.FIFTH_PRIZE);
        totalWinnings += WinCheckFragment.FIFTH_PRIZE * fifthList.size();
        view.get().displayWinnings(sixthList, WinCheckFragment.SIXTH_PRIZE);
        totalWinnings += WinCheckFragment.SIXTH_PRIZE * sixthList.size();

        if (win) {
            view.get().displayWinningsPanel(totalWinnings);
        } else {
            view.get().displaySnackBar(R.string.no_wins);
        }
    }

}
