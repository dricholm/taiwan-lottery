package com.richolm.david.taiwanlottery.listeners;

/**
 * Created by Kupo on 2016/09/18.
 */
public interface DateListener {
    void onClick(int year, int month);

    boolean onLongClick(int year, int month);

    void deleteClicked(int year, int month);
}
