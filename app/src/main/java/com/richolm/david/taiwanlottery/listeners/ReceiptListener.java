package com.richolm.david.taiwanlottery.listeners;

/**
 * Created by Kupo on 2016/08/21.
 */
public interface ReceiptListener {
    void onClick(int id, String number, int year, int month, int day);

    boolean onLongClick(int id, String number, int year, int month, int day);

    void editClicked(int id, String number, int year, int month, int day);

    void deleteClicked(int id);
}
