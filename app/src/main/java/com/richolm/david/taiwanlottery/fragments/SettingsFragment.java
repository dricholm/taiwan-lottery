package com.richolm.david.taiwanlottery.fragments;

import android.os.Bundle;
import android.support.v7.preference.PreferenceFragmentCompat;

import com.richolm.david.taiwanlottery.R;

/**
 * Created by Kupo on 2016/08/27.
 */
public class SettingsFragment extends PreferenceFragmentCompat {

    private static SettingsFragment instance;

    public static SettingsFragment newInstance() {
        if (instance == null) {
            instance = new SettingsFragment();
        }
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.settings);
    }
}