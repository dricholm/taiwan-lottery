package com.richolm.david.taiwanlottery.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.richolm.david.taiwanlottery.R;
import com.richolm.david.taiwanlottery.contracts.ScannerContract;
import com.richolm.david.taiwanlottery.database.DbProvider;
import com.richolm.david.taiwanlottery.presenters.ScannerPresenter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import icepick.Icepick;
import icepick.State;
import me.dm7.barcodescanner.zxing.ZXingScannerView;


/**
 * Created by Kupo on 2016/09/01.
 */
public class ScannerActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler, ScannerContract.View {

    public static final int ACTION_ADD = 0;
    public static final int ACTION_EDIT = 1;

    private final String TAG = getClass().getSimpleName();
    private final static int PERMISSION_CAMERA = 1;

    private ScannerContract.Presenter presenter;

    @BindView(R.id.scanner_view)
    ZXingScannerView zXingScannerView;
    @BindView(R.id.scanner_instructions)
    TextView instructionView;
    @BindView(R.id.result_text_view)
    TextView resultView;
    @BindView(R.id.bottom_buttons)
    LinearLayout bottomButtons;

    @State
    String id;
    @State
    int year;
    @State
    int month;
    @State
    int day;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);
        ButterKnife.bind(this);
        Icepick.restoreInstanceState(this, savedInstanceState);

        DbProvider dbProvider = new DbProvider(this);
        presenter = new ScannerPresenter(dbProvider);
        presenter.attachView(this);

        zXingScannerView.setResultHandler(this);

        if (id == null) {
            hideDoneView();
        } else {
            displaySuccess();
            displayDoneView(id, year, month, day);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (id != null) {
            return;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            initCamera();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                noPermission();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PERMISSION_CAMERA);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        zXingScannerView.stopCamera();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initCamera();
                    displayInstructions();
                } else {
                    noPermission();
                }
                return;
        }
    }

    private void initCamera() {
        zXingScannerView.startCamera();
        List<BarcodeFormat> barcodeFormats = new ArrayList<>();
        barcodeFormats.add(BarcodeFormat.QR_CODE);
        zXingScannerView.setFormats(barcodeFormats);
    }

    private void noPermission() {
        instructionView.setText(R.string.no_permission);
    }

    @Override
    public void handleResult(Result result) {
        Log.d(TAG, "Result: " + result.getText());
        presenter.gotResult(result.getText());
    }

    @OnClick(R.id.camera_retry)
    public void retryClicked() {
        id = null;
        presenter.retryClicked();
    }

    @OnClick(R.id.camera_edit)
    public void editClicked() {
        sendScanResult(ACTION_EDIT);
    }

    @OnClick(R.id.camera_add)
    public void addClicked() {
        sendScanResult(ACTION_ADD);
    }

    private void sendScanResult(int action) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("id", id);
        returnIntent.putExtra("year", year);
        returnIntent.putExtra("month", month);
        returnIntent.putExtra("day", day);
        returnIntent.putExtra("action", action);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    // ScannerContract.View

    @Override
    public void resumeCamera() {
        zXingScannerView.stopCamera();
        initCamera();
    }

    @Override
    public void displayInstructions() {
        instructionView.setText(R.string.scanner_instructions);
    }

    @Override
    public void displaySuccess() {
        instructionView.setText(R.string.scan_successful);
    }

    @Override
    public void displayInvalidFormat() {
        instructionView.setText(R.string.invalid_qr);
    }

    @Override
    public void hideDoneView() {
        resultView.setVisibility(View.INVISIBLE);
        bottomButtons.setVisibility(View.INVISIBLE);
    }

    @Override
    public void displayDoneView(String id, int year, int month, int day) {
        this.id = id;
        this.year = year;
        this.month = month;
        this.day = day;

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, day);
        String date = DateUtils.formatDateTime(this, calendar.getTimeInMillis(), DateUtils.FORMAT_SHOW_YEAR);
        String dayOfTheWeek = DateUtils.formatDateTime(this, calendar.getTimeInMillis(), DateUtils.FORMAT_SHOW_WEEKDAY);
        String text = date + ", " + dayOfTheWeek;
        resultView.setVisibility(View.VISIBLE);
        bottomButtons.setVisibility(View.VISIBLE);
        resultView.setText(getString(R.string.scan_results, id, text));
    }
}
