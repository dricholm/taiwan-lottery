package com.richolm.david.taiwanlottery.contracts;

/**
 * Created by Kupo on 2016/09/03.
 */
public abstract class ScannerContract {
    public interface View {
        void resumeCamera();

        void displayInstructions();

        void displaySuccess();

        void displayInvalidFormat();

        void hideDoneView();

        void displayDoneView(String id, int year, int month, int day);
    }

    public interface Presenter {
        void attachView(ScannerContract.View view);

        void detachView();

        boolean isViewAttached();

        void retryClicked();

        void addClicked();

        void gotResult(String text);
    }
}
